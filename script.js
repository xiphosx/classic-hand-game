let playerScore = 0;
let computerScore = 0;
let playerSelection = "";
let computerChoice = ["rock", "scissors", "paper"];
let game = false;

let buttons = document.querySelectorAll("button");
let text = document.getElementById("text");
let scoreBoard = document.createElement("div");
let result = document.createElement("div");
let showPlayerChoice = document.createElement("div");
let showComputerChoice = document.createElement("div");

let getResultId = document.getElementById("final-result");
let finalResultText = document.createElement("div");

// Computer choice selected 
function computerPlay() {
    return computerChoice[Math.floor(Math.random() * computerChoice.length)];
}

// Player choice selected
buttons.forEach((button) => {
    button.addEventListener("click", (e) => {
        playerSelection = button.id;

        if (!game) {
            startGame();
        }
    });
});

// Start the game
function startGame() {
    let computerSelection = computerPlay();
    gameRound(playerSelection, computerSelection);

    if (playerScore === 5) {
        game = true;
        text.removeChild(result);
        finalResultText.innerHTML = "YOU WIN!";
    } else if (computerScore === 5) {
        game = true;
        text.removeChild(result);
        finalResultText.innerHTML = "YOU LOSE";
    };
    getResultId.appendChild(finalResultText);
}

// Plag a round
function gameRound(playerSelection, computerSelection) {
    showPlayerChoice.innerHTML = `You chose: ${playerSelection}`
    showComputerChoice.innerHTML = `Computer chose: ${computerSelection}`;

    if (playerSelection === "rock") {
        if (computerSelection === "scissors") {
            playerScore++;
            showScoreBoard();
            result.innerHTML = "You win this round!";
        } else if (computerSelection === "paper") {
            computerScore++;
            showScoreBoard();
            result.innerHTML = "You lose this round.";
        } else if (computerSelection === playerSelection) {
            showScoreBoard();
            result.innerHTML = "It's a tie.";
        } else {
            result.innerHTML = "Something went wrong";
        }

    } else if (playerSelection === "paper") {
        if (computerSelection === "rock") {
            playerScore++;
            showScoreBoard();
            result.innerHTML = "You win this round!";
        } else if (computerSelection === "scissors") {
            computerScore++;
            showScoreBoard();
            result.innerHTML = "You lose this round.";
        } else if (computerSelection === playerSelection) {
            showScoreBoard();
            result.innerHTML = "It's a tie.";
        } else {
            result.innerHTML = "Something went wrong";
        }

    } else if (playerSelection === "scissors") {
        if (computerSelection === "paper") {
            playerScore++;
            showScoreBoard();
            result.innerHTML = "You win this round!";
        } else if (computerSelection === "rock") {
            computerScore++;
            showScoreBoard();
            result.innerHTML = "You lose this round.";
        } else if (computerSelection === playerSelection) {
            showScoreBoard();
            result.innerHTML = "It's a tie.";
        } else {
            result.innerHTML = "Something went wrong";
        }

    } else {
        result.innerHTML = "Something went wrong";
    }

    text.appendChild(result);
    text.appendChild(showPlayerChoice);
    text.appendChild(showComputerChoice);
    text.appendChild(scoreBoard);
}

// Keep score
function showScoreBoard() {
    scoreBoard.innerHTML = `Player Score: ${playerScore} vs Computer Score: ${computerScore}`;
}

// jQuery methods
$(document).ready(function () {
    // Method to make the current button bounce
    $("button").on("click", function () {
        $(this).first("img").effect("shake", { times: 3, direction: "up" }, 1000);
    });

    $("#final-result").css({ "color": "hsl(207, 100%, 50%", "font-size": "25px", "font-weight": "800", "text-shadow": "1px 1px 1px black" });
});